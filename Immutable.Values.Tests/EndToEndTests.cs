﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Immutable.Values.Tests
{
	public interface TestValue
	{
		int IntValue { get; }
		string StringValue { get; }
		InnerTestValue InnerTestValue { get; }

		TestValue ChangeIntValue(int newIntValue);
		TestValue ChangeStringValue(string newStringValue);
		TestValue ChangeInnerTestValue(InnerTestValue newInnerTestValue);
	}

	public interface InnerTestValue
	{
		double DoubleValue { get; }
		InnerTestValue ChangeDoubleValue(double d);
	}

	[TestClass]
	public class EndToEndTests
	{
		[TestMethod]
		public static void ClassCleanup()
		{
			ImmutableValueFactory.Debug();
		}
		
		[TestMethod]
		public void InstantiationOfCompiledImplementation()
		{
			var testValue = ImmutableValueFactory.Create<TestValue>();
			Assert.IsNotNull(testValue);
			Assert.IsInstanceOfType(testValue, typeof(TestValue));
		}

		[TestMethod]
		public void ChangeValueTypeValue()
		{
			var originalValue = ImmutableValueFactory.Create<TestValue>();
			var newValue = originalValue.ChangeIntValue(5).ChangeStringValue("Hi");
			Assert.AreEqual(5, newValue.IntValue);
			Assert.AreEqual(0, originalValue.IntValue);
			Assert.AreEqual("Hi", newValue.StringValue);
			Assert.AreEqual(null, originalValue.StringValue);
		}

		[TestMethod]
		public void ChangeReferenceTypeValue()
		{
			var originalValue = ImmutableValueFactory.Create<TestValue>();
			var newValue = originalValue.ChangeInnerTestValue(ImmutableValueFactory.Create<InnerTestValue>());

			Assert.IsNotNull(newValue.InnerTestValue);
			Assert.IsNull(originalValue.InnerTestValue);

			var updatedInnerTestValue = newValue.ChangeInnerTestValue(newValue.InnerTestValue.ChangeDoubleValue(5));

			Assert.AreEqual(5, updatedInnerTestValue.InnerTestValue.DoubleValue);
		}
	}
}
