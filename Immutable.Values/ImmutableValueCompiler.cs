﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;

namespace Immutable.Values
{
	public class ImmutableValueCompiler
	{
		private readonly ModuleBuilder _moduleBuilder;
		private readonly AssemblyBuilder _assemblyBuilder;

		public ImmutableValueCompiler(ModuleBuilder moduleBuilder, AssemblyBuilder assemblyBuilder)
		{
			_moduleBuilder = moduleBuilder;
			_assemblyBuilder = assemblyBuilder;
		}

		public void DumpNamespaceToFile()
		{
			
		}

		public static ImmutableValueCompiler Create(string namespaceName)
		{
			var assemblyBuilder = AssemblyBuilder.DefineDynamicAssembly(new AssemblyName(namespaceName), AssemblyBuilderAccess.Run);
			var moduleBuilder = assemblyBuilder.DefineDynamicModule(namespaceName + ".dll");
			return new ImmutableValueCompiler(moduleBuilder, assemblyBuilder);
		}

		public TypeInfo CompileValueInterface<T>()
		{
			var valueInterface = typeof (T);
			var fields = new List<FieldBuilder>();
			var methods = new List<MethodBuilder>();
			var typeBuilder = _moduleBuilder.DefineType(valueInterface.Name + "Impl", TypeAttributes.Class);
			typeBuilder.AddInterfaceImplementation(valueInterface);

			var constructor = typeBuilder.DefineDefaultConstructor(MethodAttributes.Public);

			foreach (var getter in valueInterface.GetProperties())
			{
				fields.Add(CreateGetter(typeBuilder, getter));				
			}

			var copyMethod = CreateCopyMethod(typeBuilder, constructor, fields);

			foreach (var changeMethod in valueInterface.GetMethods().Where(m => !m.IsSpecialName && !m.Name.StartsWith("get_")))
			{
				var associatedFieldName = changeMethod.Name.Replace("Change", "");
				associatedFieldName = "_" + Char.ToLower(associatedFieldName[0]) + associatedFieldName.Substring(1);
				methods.Add(CreateMethod(typeBuilder, changeMethod, copyMethod, fields.First(f => f.Name == associatedFieldName)));
			}

			return typeBuilder.CreateTypeInfo();
		}

		private FieldBuilder CreateGetter(TypeBuilder typeBuilder, PropertyInfo getter)
		{
			var fieldName = "_" + Char.ToLower(getter.Name[0]) + getter.Name.Substring(1);
			var fieldBuilder = typeBuilder.DefineField(fieldName, getter.PropertyType, FieldAttributes.Private);
			var methodBuilder = typeBuilder.DefineMethod("get_" + getter.Name, MethodAttributes.Public | MethodAttributes.Virtual);
			methodBuilder.SetReturnType(getter.PropertyType);

			var generator = methodBuilder.GetILGenerator();
			generator.Emit(OpCodes.Ldarg_0);
			generator.Emit(OpCodes.Ldfld, fieldBuilder);
			generator.Emit(OpCodes.Ret);

			return fieldBuilder;
		}

		private MethodBuilder CreateMethod(TypeBuilder typeBuilder, MethodInfo methodInfo, MethodBuilder copyMethod, FieldBuilder associatedFieldBuilder)
		{
			MethodBuilder methodBuilder = typeBuilder.DefineMethod(methodInfo.Name, MethodAttributes.Public | MethodAttributes.Virtual);
			methodBuilder.SetParameters(methodInfo.GetParameters().ToList().Select(p => p.ParameterType).ToArray());
			methodBuilder.SetReturnType(methodInfo.ReturnType);

			var generator = methodBuilder.GetILGenerator();
			var copy = generator.DeclareLocal(typeBuilder.AsType());

			generator.Emit(OpCodes.Ldarg_0);
			generator.Emit(OpCodes.Call, copyMethod);
			
			generator.EmitLocalStore(copy);

			generator.EmitLocalLoad(copy);
			generator.EmitArgLoad(1);
			generator.Emit(OpCodes.Stfld, associatedFieldBuilder);

			generator.EmitLocalLoad(copy);
			generator.Emit(OpCodes.Ret);	
			
			return methodBuilder;
		}

		private MethodBuilder CreateCopyMethod(TypeBuilder typeBuilder, ConstructorBuilder constructor, List<FieldBuilder> fields)
		{
			var methodBuilder = typeBuilder.DefineMethod("Copy", MethodAttributes.Private);
			methodBuilder.SetReturnType(typeBuilder.AsType());

			var generator = methodBuilder.GetILGenerator();
			var copy = generator.DeclareLocal(typeBuilder.AsType());
			generator.Emit(OpCodes.Newobj, constructor);
			generator.EmitLocalStore(copy);

			foreach (var field in fields)
			{
				generator.EmitLocalLoad(copy);
				generator.Emit(OpCodes.Ldarg_0);
				generator.Emit(OpCodes.Ldfld, field);
				generator.Emit(OpCodes.Stfld, field);
			}

			generator.EmitLocalLoad(copy);
			generator.Emit(OpCodes.Ret);
			
			return methodBuilder;
		}
	}
}