﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;

namespace Immutable.Values
{
	public static class ILGeneratorExtensions
	{
		public static void EmitArgLoad(this ILGenerator generator, int index)
		{
			if (index == 0) generator.Emit(OpCodes.Ldarg_0);
			else if (index == 1) generator.Emit(OpCodes.Ldarg_1);
			else if (index == 2) generator.Emit(OpCodes.Ldarg_2);
			else if (index == 3) generator.Emit(OpCodes.Ldarg_3);
			else generator.Emit(OpCodes.Ldarg, index);
		}

		public static void EmitLocalLoad(this ILGenerator generator, LocalBuilder builder)
		{
			if (builder.LocalIndex == 0) generator.Emit(OpCodes.Ldloc_0);
			else if (builder.LocalIndex == 1) generator.Emit(OpCodes.Ldloc_1);
			else if (builder.LocalIndex == 2) generator.Emit(OpCodes.Ldloc_2);
			else if (builder.LocalIndex == 3) generator.Emit(OpCodes.Ldloc_3);
			else generator.Emit(OpCodes.Ldloc, builder.LocalIndex);
		}

		public static void EmitLocalStore(this ILGenerator generator, LocalBuilder builder)
		{
			if (builder.LocalIndex == 0) generator.Emit(OpCodes.Stloc_0);
			else if (builder.LocalIndex == 1) generator.Emit(OpCodes.Stloc_1);
			else if (builder.LocalIndex == 2) generator.Emit(OpCodes.Stloc_2);
			else if (builder.LocalIndex == 3) generator.Emit(OpCodes.Stloc_3);
			else generator.Emit(OpCodes.Stloc, builder.LocalIndex);
		}

		public static void EmitInt(this ILGenerator generator, int x)
		{
			if (x == 0) generator.Emit(OpCodes.Ldc_I4_0);
			else if (x == 1) generator.Emit(OpCodes.Ldc_I4_1);
			else if (x == 2) generator.Emit(OpCodes.Ldc_I4_2);
			else if (x == 3) generator.Emit(OpCodes.Ldc_I4_3);
			else generator.Emit(OpCodes.Ldc_I4, x);
		}

		public static void EmitType(this ILGenerator generator, Type type)
		{
			generator.Emit(OpCodes.Ldtoken, type);
			generator.Emit(OpCodes.Call, typeof(Type).GetMethod("GetTypeFromHandle"));
		}

		public static void EmitBoxedInt(this ILGenerator generator, object i)
		{
			generator.EmitInt((int)i);
			generator.Emit(OpCodes.Box, typeof(Int32));
		}

		public static void EmitBoxedBoolean(this ILGenerator generator, object b)
		{
			if ((bool)b) generator.EmitInt(1);
			else generator.EmitInt(0);
			generator.Emit(OpCodes.Box, typeof(Boolean));
		}

		public static void EmitBoxedDouble(this ILGenerator generator, object d)
		{
			generator.Emit(OpCodes.Ldc_R8, (double)d);
			generator.Emit(OpCodes.Box, typeof(Double));
		}

		public static void EmitArray<T>(this ILGenerator generator, List<T> arrayElements, Action<T> elementEmitAction)
		{
			generator.EmitInt(arrayElements.Count);
			generator.Emit(OpCodes.Newarr, typeof(object));

			if (arrayElements.Count > 0)
			{
				var arrayToHoldArguments = generator.DeclareLocal(typeof(object[]));
				generator.EmitLocalStore(arrayToHoldArguments);

				for (var i = 0; i < arrayElements.Count; i++)
				{
					generator.EmitLocalLoad(arrayToHoldArguments);
					generator.EmitInt(i);
					elementEmitAction(arrayElements[i]);
					generator.Emit(OpCodes.Stelem_Ref);
				}

				generator.EmitLocalLoad(arrayToHoldArguments);
			}
		}

		public static void EmitTailCall(this ILGenerator generator, OpCode op, MethodInfo method)
		{
			generator.Emit(OpCodes.Tailcall);
			generator.Emit(op, method);
			generator.Emit(OpCodes.Ret);
		}
	}
}