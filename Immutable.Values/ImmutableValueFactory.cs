﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Immutable.Values
{
	public class ImmutableValueFactory
	{
		private static readonly ImmutableValueCompiler _compiler;
		private static readonly Dictionary<Type, TypeInfo> _knownImplementations;

		static ImmutableValueFactory()
		{
			_knownImplementations = new Dictionary<Type, TypeInfo>();
			_compiler = ImmutableValueCompiler.Create("ImmutableValues.Compiled");
		}

		public static T Create<T>() where T : class
		{
			if (!_knownImplementations.ContainsKey(typeof (T)))
			{
				var compiledImplementation = _compiler.CompileValueInterface<T>();
				_knownImplementations.Add(typeof (T), compiledImplementation);
			}

			var implementation = _knownImplementations[typeof (T)];
			return Activator.CreateInstance(implementation.AsType()) as T;
		}

		public static void Debug()
		{
			_compiler.DumpNamespaceToFile();
		}
	}
}