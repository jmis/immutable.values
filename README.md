Immutable.Values
================

This .NET Immutable value compiler looks at user-defined interfaces at runtime and generates the IL for immutability.

### Interface Example

    public interface TestValue
    {
      int IntValue { get; }
      string StringValue { get; }
  
      TestValue ChangeIntValue(int newIntValue);
      TestValue ChangeStringValue(string newStringValue);
    }

### Usage example

    var newTestValue = ImmutableValueFactory.Create<TestValue>()
      .ChangeIntValue(5)
      .ChangeStringValue("Hello World.");
